namespace Game
{
    public class LetterStates : Dictionary<char, LetterState>
    {
        public void Absorb(LetterStates other)
        {
            foreach ((var c, var otherState) in other)
            {
                var newState = otherState.Clone();

                if (this.TryGetValue(c, out var myState))
                {
                    newState.NotPositions.AddRange(myState.NotPositions);
                }

                this[c] = newState;
            }
        }

        public bool WordMatches(string word)
        {
            foreach ((var letter, var state) in this)
            {
                if (word.Contains(letter) != state.IsInWord ||
                    state.Positions.Any(p => word[p] != letter) ||
                    state.NotPositions.Any(p => word[p] == letter))
                {
                    return false;
                }
            }

            return true;
        }
    }
}