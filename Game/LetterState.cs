namespace Game
{
    public class LetterState
    {
        public bool IsInWord { get; internal set; } = false;

        public List<int> Positions { get; internal set; } = new List<int>();

        public List<int> NotPositions { get; internal set; } = new List<int>();

        public LetterState Clone()
        {
            return new LetterState()
            {
                IsInWord = this.IsInWord,
                Positions = this.Positions.ToList(),
                NotPositions = this.NotPositions.ToList(),
            };
        }
    }
}