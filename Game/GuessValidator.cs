namespace Game
{
    public class GuessValidator
    {
        private string Word { get; }

        public int WordLength { get => this.Word.Length; }

        public GuessValidator(string word)
        {
            this.Word = word.ToLower();
        }

        public (bool won, LetterStates letterStates) TestGuess(string guess)
        {
            guess = guess.ToLower();

            if (string.IsNullOrEmpty(guess))
            {
                throw new GameException("Guess must be non-empty string");
            }

            if (guess.Length != this.Word.Length)
            {
                throw new GameException("Guess length must mach word length");
            }

            var letterStates = this.DetermineLetterStates(guess);

            return (guess == this.Word, letterStates);
        }

        private LetterStates DetermineLetterStates(string guess)
        {
            var letterStates = new LetterStates();

            for (int i = 0; i < guess.Length; i++)
            {
                char guessChar = guess[i];
                var state = letterStates.GetValueOrDefault(guessChar) ?? new LetterState();

                if (guessChar == this.Word[i])
                {
                    state.IsInWord = true;
                    state.Positions.Add(i);
                }
                else if (this.Word.Contains(guessChar))
                {
                    state.IsInWord = true;
                    state.NotPositions.Add(i);
                }

                letterStates[guessChar] = state;
            }

            return letterStates;
        }
    }
}