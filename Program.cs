﻿using Solver;

SolverEvaluator.EvaluateSolvers(new BaseSolver[]
{
    new RandomWordSolver(),
    new MaxVowelsSolver(),
    new MaxUnusedSolver(),
    new MaxVowelsAndUnusedSolver(),
});