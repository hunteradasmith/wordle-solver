using Game;

namespace Solver
{
    public class MaxUnusedSolver : MaxVowelsSolver
    {
        protected override string NextGuess(LetterStates currentStates, int length)
        {
            var words = WordSelectors.MatchingWords(currentStates, length);
            var byUnused = words.GroupBy(w =>
                this.UnguessedInWord(currentStates, w));

            var bestWords = byUnused.OrderByDescending(g => g.Key).First();
            return WordSelectors.RandomChoice(bestWords);
        }

        protected int UnguessedInWord(LetterStates states, string word) =>
            word.Where(c => !states.Keys.Contains(c)).Count();
    }
}
