using Game;

namespace Solver
{
    public class RandomWordSolver : BaseSolver
    {
        protected override string NextGuess(LetterStates currentStates, int length)
        {
            var words = WordSelectors.MatchingWords(currentStates, length);
            return WordSelectors.RandomChoice(words);
        }
    }
}
