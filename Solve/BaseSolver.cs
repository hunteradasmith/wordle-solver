using Game;

namespace Solver
{
    public abstract class BaseSolver
    {
        private const int _maxGuesses = 6;

        public SolutionStats Solve(GuessValidator validator)
        {
            var stats = new SolutionStats();
            var currentStates = new LetterStates();

            for (var i = 0; i < _maxGuesses; i++)
            {
                var guess = this.NextGuess(currentStates, validator.WordLength);
                (var won, var newStates) = validator.TestGuess(guess);

                stats.Solved = won;
                stats.GuessCount++;
                stats.GuessWords.Add(guess);
                stats.GuessStates.Add(newStates);

                currentStates.Absorb(newStates);

                if (won) break;
            }

            return stats;
        }

        protected abstract string NextGuess(LetterStates currentStates, int length);
    }
}