using Game;

namespace Solver
{
    public static class SolverEvaluator
    {
        public static void EvaluateSolvers(
            IEnumerable<BaseSolver> solvers,
            int wordLength = 5,
            int maxGuesses = 6,
            int solverIterations = 1000)
        {
            foreach (var solver in solvers)
            {
                var successes = new List<SolutionStats>();
                var failures = new List<SolutionStats>();

                for (var i = 0; i < solverIterations; i++)
                {
                    var gv = new GuessValidator(WordSelectors.RandomWordOfLength(5));
                    var stats = solver.Solve(gv);

                    if (stats.Solved)
                        successes.Add(stats);
                    else
                        failures.Add(stats);
                }

                var avg = successes.Select(s => s.GuessCount).Average();

                Console.WriteLine($"{solver.GetType().Name,30}: {successes.Count,5} Wins {failures.Count,5} Losses {Math.Round(avg, 4),6} Guess Average");
            }
        }
    }
}