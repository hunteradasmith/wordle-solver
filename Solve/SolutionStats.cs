using Game;

namespace Solver
{
    public class SolutionStats
    {
        public bool Solved { get; set; } = false;

        public int GuessCount { get; set; } = 0;

        public List<string> GuessWords { get; set; } = new List<string>();

        public List<LetterStates> GuessStates { get; set; } = new List<LetterStates>();

        public override string ToString() =>
            $"Solved: {this.Solved} | GuessCount: {this.GuessCount} | Words: {string.Join(',', this.GuessWords)}";
    }
}