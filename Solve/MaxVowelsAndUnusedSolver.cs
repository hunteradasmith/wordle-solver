using Game;

namespace Solver
{
    public class MaxVowelsAndUnusedSolver : MaxVowelsSolver
    {
        protected override string NextGuess(LetterStates currentStates, int length)
        {
            var words = WordSelectors.MatchingWords(currentStates, length);
            var byVowelsAndUnguessed = words.GroupBy(w =>
                this.VowelsInWord(w) + this.UnguessedInWord(currentStates, w));

            var bestWords = byVowelsAndUnguessed.OrderByDescending(g => g.Key).First();
            return WordSelectors.RandomChoice(bestWords);
        }

        protected int UnguessedInWord(LetterStates states, string word) =>
            word.Where(c => !states.Keys.Contains(c)).Count();
    }
}
