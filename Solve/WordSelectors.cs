using Game;

namespace Solver
{
    public static class WordSelectors
    {
        private const string _wordsFilePath = "/usr/share/dict/words";

        private static Dictionary<int, HashSet<string>> _wordsByLength;

        static WordSelectors()
        {
            _wordsByLength = new Dictionary<int, HashSet<string>>();
            using var wordsFile = File.OpenText(_wordsFilePath);

            string? line;
            while ((line = wordsFile.ReadLine()) != null)
            {
                if (string.IsNullOrWhiteSpace(line) ||
                    !line.All(c => char.IsLetter(c)))
                {
                    continue;
                }

                var word = line.Trim().ToLower();

                if (!_wordsByLength.ContainsKey(word.Length))
                {
                    _wordsByLength[word.Length] = new HashSet<string>();
                }

                _wordsByLength[word.Length].Add(word);
            }
        }

        public static string[] MatchingWords(LetterStates states, int length)
        {
            var wordsOfLength = _wordsByLength[length];
            var matching = wordsOfLength.Where(word =>
                states.WordMatches(word));

            return matching.ToArray();
        }

        public static string RandomWordOfLength(int length) =>
            RandomChoice(_wordsByLength[length]);

        public static string RandomChoice(IEnumerable<string> words)
        {
            var rnd = new Random();
            int index = rnd.Next(words.Count());
            return words.ElementAt(index);
        }
    }
}