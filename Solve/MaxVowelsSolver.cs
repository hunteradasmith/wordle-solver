using Game;

namespace Solver
{
    public class MaxVowelsSolver : BaseSolver
    {
        private const string _vowels = "aeiou";

        protected override string NextGuess(LetterStates currentStates, int length)
        {
            var words = WordSelectors.MatchingWords(currentStates, length);
            var byVowels = words.GroupBy(w => this.VowelsInWord(w));

            var bestWords = byVowels.OrderByDescending(g => g.Key).First();
            return WordSelectors.RandomChoice(bestWords);
        }

        protected int VowelsInWord(string word) =>
            word.Where(c => _vowels.Contains(c)).Count();
    }
}
