# wordle-solver

This is a for-fun solver program for the viral world game [Wordle by Josh Wardle](https://en.wikipedia.org/wiki/Wordle). When playing the game, I was curious as to what word-selection strategy would be most efficient. This program is an attempt to answer that, though I doubt I've reached anywhere near the most-effecient solution algorithm.

The program uses the system spell check dictionary for words to guess. This is likely a larger list of possible words than the official Wordle game uses. Also, due to a hard-coded words file path, this program currently only run on Linux.

## Results
```
              RandomWordSolver:   928 Wins    72 Losses 4.3728 Guess Average
               MaxVowelsSolver:   841 Wins   159 Losses 4.6623 Guess Average
               MaxUnusedSolver:   919 Wins    81 Losses  4.395 Guess Average
      MaxVowelsAndUnusedSolver:   837 Wins   163 Losses 4.5866 Guess Average
```

Surpringly, the "choose words at random" solver algorithm seems to be most efficent out of the algorithms I implemented. There's likely room to improve, but for now I don't intend to contine working on this project.